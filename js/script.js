
// Our Services tab

$(".services-list").click(function() {
    $(".services-list").removeClass("active");
    $(".services-content").removeClass("active");
    $(this).addClass("active");
    $("#"+$(this).attr("data-target")).addClass("active");
});


// $(function () {
//     $('ul.services-tab').on('click', 'li:not(.active)', function () {
//         $(this)
//             .addClass('active').siblings().removeClass('active')
//             .closest('div.services').find('div.services-content').removeClass('active').eq($(this).index()).addClass('active');
//     });
// });

// Isotope

// $(document).ready(function() {
            
//     $('.portfolio-grid').isotope({
//         itemSelector: '.single-content',
//         layoutMode: 'fitRows'
//     });
    
//     $('.portfolio-menu ul li').click(function() {
//         $('.portfolio-menu ul li').removeClass('active');
//         $(this).addClass('active');

//         const selector = $(this).attr('data-filter');
//         $('.portfolio-grid').isotope({
//             filter: selector
//         });
//         return false;
//     });
// })

// Filter

$(document).ready(function() {
  $('.category-items').click(function() {
    let category = $(this).attr('id');

    if (category == 'all') {
      $('.element-item').addClass('hide');
      setTimeout(function() {
        $('element-item').removeClass('hide')
      }, 300)
    } else {
      $('.element-item').addClass('hide');
      setTimeout(function() {
        $('.' + category).removeClass('hide')
      }, 300)
    }

  })
})




// Load More

$(document).ready(function(){
    $(".single-content").slice(0, 0).show();
    $("#loadMore").on("click", function(e){
      e.preventDefault();
      $(".single-content:hidden").slice(0, 12).slideDown();
      if($(".single-content:hidden").length == 0) {
        $("#loadMore").addClass("noContent");
      }
    });
    
  })


// Slider

$('.main-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.nav-slider'
});
$('.nav-slider').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '.main-slider',
  dots: false,
  arrows: true,
  centerMode: true,
  //infinite: true,
  focusOnSelect: true
});